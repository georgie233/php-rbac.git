SET
FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `###permission_category`;
CREATE TABLE `###permission_category`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(50) COLLATE utf8mb4_general_ci  NOT NULL DEFAULT '' COMMENT '权限分组名称',
    `description` varchar(200) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限分组描述',
    `status`      smallint(4) unsigned NOT NULL DEFAULT '1' COMMENT '权限分组状态1有效0无效',
    `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '权限分组创建时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
DROP TABLE IF EXISTS `###permission`;
DROP TABLE IF EXISTS `###permission`;
CREATE TABLE `###permission`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(50)  NOT NULL DEFAULT '' COMMENT '权限节点名称',
    `type`        smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '权限类型1接口权限2菜单权限',
    `category_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '权限分组id',
    `path`        varchar(100) NOT NULL DEFAULT '' COMMENT '权限路径',
    `description` varchar(200) NOT NULL DEFAULT '' COMMENT '描述信息',
    `status`      smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态0停用1正常',
    `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
    PRIMARY KEY (`id`),
    KEY           `idx_permission` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限节点';
DROP TABLE IF EXISTS `###organize`;
CREATE TABLE `###organize`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(50)  NOT NULL DEFAULT '' COMMENT '组织名',
    `description` varchar(200) NOT NULL DEFAULT '' COMMENT '组织描述',
    `status`      smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态0停用1正常',
    `p_id`        int(11) NOT NULL DEFAULT '0' COMMENT '父组织',
    `p_id_all`    varchar(200) NOT NULL DEFAULT '' COMMENT '父组织集合',
    PRIMARY KEY (`id`),
    KEY           `idx_organize` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='组织';
DROP TABLE IF EXISTS `###position`;
CREATE TABLE `###position`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(50)  NOT NULL DEFAULT '' COMMENT '职位名称',
    `description` varchar(200) NOT NULL DEFAULT '' COMMENT '职位描述',
    `status`      smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态0停用1正常',
    PRIMARY KEY (`id`),
    KEY           `idx_position` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='职位';
DROP TABLE IF EXISTS `###user_group`;
CREATE TABLE `###user_group`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(50)  NOT NULL DEFAULT '' COMMENT '用户组名',
    `description` varchar(200) NOT NULL DEFAULT '' COMMENT '用户组描述',
    `status`      smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态0停用1正常',
    PRIMARY KEY (`id`),
    KEY           `idx_user_group` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户组';
DROP TABLE IF EXISTS `###role`;
CREATE TABLE `###role`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`        varchar(50)  NOT NULL DEFAULT '' COMMENT '角色名',
    `description` varchar(200) NOT NULL DEFAULT '' COMMENT '角色描述',
    `status`      smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态0停用1正常',
    PRIMARY KEY (`id`),
    KEY           `idx_role` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色';
DROP TABLE IF EXISTS `###user`;
CREATE TABLE `###user`
(
    `id`              int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`            varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
    `password`        varchar(64) NOT NULL DEFAULT '' COMMENT '用户密码',
    `phone`           varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
    `nick_name`       varchar(50) NOT NULL DEFAULT '' COMMENT '用户名',
    `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后一次登录时间',
    `status`          smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态0禁用1正常',
    `create_time`     int(10) unsigned NOT NULL DEFAULT '0' COMMENT '账号创建时间',
    `update_time`     int(10) unsigned NOT NULL DEFAULT '0' COMMENT '信息更新时间',
    PRIMARY KEY (`id`),
    KEY               `idx_user` (`name`,`phone`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';
DROP TABLE IF EXISTS `###user_organize`;
CREATE TABLE `###user_organize`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
    `organize_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '组织id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户组织关联表';
DROP TABLE IF EXISTS `###user_position`;
CREATE TABLE `###user_position`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`     int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
    `position_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '职位id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户职位关联表';
DROP TABLE IF EXISTS `###user_user_group`;
CREATE TABLE `###user_user_group`
(
    `id`       int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id`  int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
    `group_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户组id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户-用户组关联表';
DROP TABLE IF EXISTS `###organize_role`;
CREATE TABLE `###organize_role`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `organize_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '组织id',
    `role_id`     int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='组织角色对应关系';
DROP TABLE IF EXISTS `###position_role`;
CREATE TABLE `###position_role`
(
    `id`          int(11) unsigned NOT NULL AUTO_INCREMENT,
    `position_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '组织id',
    `role_id`     int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='职位角色对应关系';
DROP TABLE IF EXISTS `###user_group_role`;
CREATE TABLE `###user_group_role`
(
    `id`       int(11) unsigned NOT NULL AUTO_INCREMENT,
    `group_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户组id',
    `role_id`  int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户组角色对应关系';
DROP TABLE IF EXISTS `###user_role`;
CREATE TABLE `###user_role`
(
    `id`      int(11) unsigned NOT NULL AUTO_INCREMENT,
    `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
    `role_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户角色对应关系';
DROP TABLE IF EXISTS `###role_permission`;
CREATE TABLE `###role_permission`
(
    `id`            int(11) unsigned NOT NULL AUTO_INCREMENT,
    `role_id`       int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
    `permission_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '权限id',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色权限对应关系';