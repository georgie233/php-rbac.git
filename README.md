> 本扩展包是php的rbac包，在TP5进行测试
## 安装方法
```
composer require georgie/php-rbac dev-master
```

由于依赖了think-orm和think-cache,需要进行配置

如果使用的是thinkphp框架请参考:
* [thinkphp-数据库配置](https://www.kancloud.cn/manual/thinkphp5_1/353998)
* [thinkphp-缓存配置](https://www.kancloud.cn/manual/thinkphp5_1/354116)

### php使用
```injectablephp
Cache::config([
    'default' => 'file',
    'stores' => [
        'file' => [
            'type' => 'File',
            // 缓存保存目录
            'path' => './cache/',
            // 缓存前缀
            'prefix' => '',
            // 缓存有效期 0表示永久缓存
            'expire' => 0,
        ],
        'redis' => [
            'type' => 'redis',
            'host' => '127.0.0.1',
            'port' => 6379,
            'prefix' => '',
            'expire' => 0,
        ],
    ],
]);
Db::setConfig([
    'default' => 'mysql',
    'connections' => [
        'mysql' => [

            // 数据库类型
            'type' => 'mysql',
            // 主机地址
            'hostname' => 'www.georgie.cn',
            // 数据库名
            'database' => 'php_rpac_test',
            // 用户名密码
            'username' => 'php_rpac_test',
            'password' => 'dZWrXAsazZzKhfmP',
            // 数据库编码默认采用utf8
            'charset' => 'utf8',
            // 数据库表前缀
            'prefix' => 'think_',
            // 数据库调试模式
            'debug' => true,
        ]
    ]
]);

$rbac = new Rbac();
```